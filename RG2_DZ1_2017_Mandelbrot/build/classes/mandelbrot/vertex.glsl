#version 450 core

layout(location = 0) in vec2 vertexPosition;

uniform float scale;
uniform float translateX;
uniform float translateY;

vec2 vertexPositionTransformed;
out vec2 vertexPositionOriginal;
        
void main() {    
    vertexPositionTransformed = vertexPosition;
    vertexPositionTransformed += vec2(-translateX, -translateY);
    vertexPositionTransformed *= scale;            
    
    
    gl_Position = vec4(vertexPositionTransformed, 0.0, 1.0);    
    vertexPositionOriginal = vertexPosition;
}
