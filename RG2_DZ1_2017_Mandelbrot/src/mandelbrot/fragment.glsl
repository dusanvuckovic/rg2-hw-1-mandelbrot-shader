#version 450 core

in vec2 vertexPositionOriginal;
out vec4 color;

uniform int WINDOW_WIDTH;
uniform int WINDOW_HEIGHT;
uniform int maxIt;

float transformRanges(in float oldValue, in float oldMin,
        in float oldMax, in float newMin, in float newMax) {
        return newMin + (oldValue - oldMin) / (oldMax - oldMin) * (newMax - newMin);                
}
    
float transformX(in float oldValue) {
    return transformRanges(oldValue, -1, 1, -2.5, 1);
}

float transformY(in float oldValue) {
    return transformRanges(oldValue, -1, 1, -1, 1);
}

float transformColor(in float oldValue) {
    return transformRanges(oldValue, 0, 360, 0, 1);
}

/* http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl */
vec3 hsv2rgb(in vec3 c) {
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {

    vec2 vertexPosition = vertexPositionOriginal;

    float x0 = transformX(vertexPosition.x);
    float y0 = transformY(vertexPosition.y);
    float x = 0;
    float y = 0;
    int it = 0;
    
    while ((x*x + y*y) < 4 && (it < maxIt)) {
        float xTemp = x*x - y*y + x0;
        y = 2*x*y + y0;
        x = xTemp;
        it++;
    }
    color = (it == maxIt) ? vec4(0, 0, 0, 1) : vec4(hsv2rgb(vec3(transformColor(it), 1, 1)), 1);            
        
}