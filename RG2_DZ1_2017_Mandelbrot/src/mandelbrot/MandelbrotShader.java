package mandelbrot;

import com.jogamp.opengl.GL4;
import java.io.IOException;
import java.nio.file.Paths;
import ogl4.shader.FragmentShader;
import ogl4.shader.ShaderProgram;
import ogl4.shader.VertexShader;

public class MandelbrotShader extends ComboVertexFragmentShader {
    
    private String pathToVertex, pathToFragment;
    
    public MandelbrotShader(String pathToVertex, String pathToFragment) {
        super("Mandelbrot Shader");          
        this.pathToVertex = pathToVertex;
        this.pathToFragment = pathToFragment;
    }
    
    @Override
    public void buildShader(GL4 gl) {
        
        vertexShader = new VertexShader(String.format("%s VS", "Mandelbrot Shader"));
        fragmentShader = new FragmentShader(String.format("%s FS", "Mandelbrot Shader"));
        shaderProgram = new ShaderProgram("Mandelbrot Shader");

        try {
            vertexShader.readSource(Paths.get(pathToVertex));       
            fragmentShader.readSource(Paths.get(pathToFragment));
            
            shaderProgram.addShader(vertexShader);
            shaderProgram.addShader(fragmentShader);
            shaderProgram.build(gl);
            
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("Error: bad shader path filenames!");            
        }
        System.out.println(shaderProgram.getLog());
        
    }
    
}
