package mandelbrot;

import com.jogamp.opengl.GL4;
import java.io.IOException;
import java.nio.file.Path;
import ogl4.shader.FragmentShader;
import ogl4.shader.ShaderProgram;
import ogl4.shader.VertexShader;

public class ComboVertexFragmentShader {

    private String vertexShaderSrc
            = "#version 430 core \n"
            + "layout(location = 0) in vec2 vertexPosition;\n"
            + "void main()\n"
            + "{\n"
            + "gl_Position = vec4(vertexPosition, 0.0, 1.0);\n"
            + "}\n";

    private String fragmentShaderSrc
            = "#version 430\n"
            + "out vec4 outColor2;\n"
            + "out vec4 outColor;\n"
            + "void main()\n"
            + "{\n"
            + "outColor = vec4(1.0, 1.0, 1.0, 1.0);\n"
            + "outColor2 = vec4(1.0, 0, 0, 1.0);\n"
            + "}\n";

    protected VertexShader vertexShader;
    protected FragmentShader fragmentShader;
    protected ShaderProgram shaderProgram;
    
    private String title;
    
    public ComboVertexFragmentShader(String title) {
        this.title = title;
    }

    public int getProgramObjectID() {
        return shaderProgram.getID();
    }

    public void buildShader(GL4 gl) {
        vertexShader = new VertexShader(String.format("%s VS", title));
        fragmentShader = new FragmentShader(String.format("%s FS", title));
        shaderProgram = new ShaderProgram(title);

        vertexShader.setSource(vertexShaderSrc);
        fragmentShader.setSource(fragmentShaderSrc);

        shaderProgram.addShader(vertexShader);
        shaderProgram.addShader(fragmentShader);

        shaderProgram.build(gl);
        System.out.println(shaderProgram.getLog());
    }

}
