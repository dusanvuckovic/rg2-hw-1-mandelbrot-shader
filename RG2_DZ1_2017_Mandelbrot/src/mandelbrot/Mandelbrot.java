package mandelbrot;

import com.jogamp.common.nio.Buffers;
import com.jogamp.newt.event.WindowAdapter;
import com.jogamp.newt.event.WindowEvent;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GL4bc;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.FPSAnimator;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Scanner;

public class Mandelbrot implements GLEventListener {

    private static final int FPS = 60;
    private static int WINDOW_WIDTH = 1000;
    private static int WINDOW_HEIGHT = 800;
    private static final String TITLE = "Mandelbrot";

    private static int maxIt = 500;

    private int programID;
    private int vertexArrayID;
    private int bufferArrayID;
        
    private int translateXLocation,
            translateYLocation,
            scaleLocation;
    private int windowWidthLocation,
            windowHeightLocation,
            maxItLocation;

    private float translateX = 0,
            translateY = 0,
            scaleFactor = 1.0f;
    
    private NumberThread numbers;

    float[] vertices = {
        -1.00f, -1.00f, // Triangle 1
        -1.00f, 1.00f,
        1.00f, -1.00f,
        1.00f, 1.00f, // Triangle 2
        -1.00f, 1.00f,
        1.00f, -1.00f
    };        
    
    public Mandelbrot() {
        numbers = new NumberThread();        
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        GL4bc gl = drawable.getGL().getGL4bc();

        /**
         * Pravi bafer celih brojeva koji čuva celobrojne identifikatore bafera
         * koje koristi OGL (boje, temena...). Ovde koristimo jedan bafer pa je
         * veličina jedan.
         */
        IntBuffer intBuffer = IntBuffer.allocate(1);

        /**
         * Pravi bafer podataka o temenima koji čuva float brojeve koji određuju
         * temena primitive. Povezuje taj bafer sa svojom pozicijom. Pozivom
         * glGenBuffers u intBuffer dobijamo novi ID, ID bafera. Sa glBindBuffer
         * menjamo stanje OGL-a tako da je naš bafer tekući a sa glBufferData
         * kopiramo podatke iz floatBuffer-a (klijent) na bufferArrayID bafer
         * (server).
         */
        FloatBuffer floatBuffer = Buffers.newDirectFloatBuffer(vertices);
        gl.glGenBuffers(1, intBuffer);
        bufferArrayID = intBuffer.get(0);
        intBuffer.rewind();
        gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, bufferArrayID);
        gl.glBufferData(GL4.GL_ARRAY_BUFFER, vertices.length * Float.BYTES,
                floatBuffer, GL4.GL_STATIC_DRAW);

        /**
         * Pravi bafer celih brojeva koji čuva celobrojne identifikatore bafera
         * koje koristi OGL (boje, temena...). Ovde koristimo jedan bafer pa je
         * veličina jedan. Nakon poziva glGenVertexArrays u intBUffer se nalazi
         * jedan ceo broj, ID našeg Vertex Array Object-a (VAO). Nakon toga
         * "premotavamo" bafer nazad jer operacija get() inkrementira njegovo
         * interno polje position. Pozivom bindVertexArray stvaramo i aktiiramo
         * novo ime.
         */
        gl.glGenVertexArrays(1, intBuffer);
        vertexArrayID = intBuffer.get(0);
        intBuffer.rewind();
        gl.glBindVertexArray(vertexArrayID);

        /**
         * F-ja glVertexAttribPointer povezuje promenljivu šejdera sa lokacijom
         * nula, koja prima po 2 parametra tipa FLOAT bez normalizacije, sa
         * nultim razmakom između podataka i nula-ofsetom. F-ja
         * glEnableVertexAttribArray(0) "pali" tu vezu, što dozvoljava podacima
         * da teku.
         */
        gl.glVertexAttribPointer(0, 2, GL4.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(0);
        
        MandelbrotShader shader = new MandelbrotShader("src/mandelbrot/vertex.glsl", "src/mandelbrot/fragment.glsl");
        shader.buildShader(gl);        

        numbers.start();
        
        translateX = 0;
        translateY = 0;
        scaleFactor = 1.0f;
        programID = shader.getProgramObjectID();
        scaleLocation = gl.glGetUniformLocation(programID, "scale");
        translateXLocation = gl.glGetUniformLocation(programID, "translateX");
        translateYLocation = gl.glGetUniformLocation(programID, "translateY");
        windowWidthLocation = gl.glGetUniformLocation(programID, "WINDOW_WIDTH");
        windowHeightLocation = gl.glGetUniformLocation(programID, "WINDOW_HEIGHT");
        maxItLocation = gl.glGetUniformLocation(programID, "maxIt");

    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
        GL4 gl = drawable.getGL().getGL4();
        IntBuffer ib = Buffers.newDirectIntBuffer(1);
        ib.put(0, bufferArrayID);
        gl.glDeleteBuffers(1, ib);
        ib.put(0, vertexArrayID);
        ib.rewind();
        gl.glDeleteVertexArrays(1, ib);
        gl.glDeleteProgram(programID);
        System.out.println("Kraj!");
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        GL4 gl = drawable.getGL().getGL4();
        
        maxIt = numbers.getMaxIt();

        gl.glClear(gl.GL_COLOR_BUFFER_BIT);
        gl.glBindVertexArray(vertexArrayID);
        gl.glDrawArrays(gl.GL_TRIANGLES, 0, vertices.length);

        gl.glUseProgram(programID);
        gl.glUniform1f(scaleLocation, scaleFactor);
        gl.glUniform1f(translateXLocation, translateX);
        gl.glUniform1f(translateYLocation, translateY);
        gl.glUniform1i(windowHeightLocation, WINDOW_HEIGHT);
        gl.glUniform1i(windowWidthLocation, WINDOW_WIDTH);
        gl.glUniform1i(maxItLocation, maxIt);

        gl.glFlush();
    }

    private float getScaleBorder() {
        return 1 - 1 / scaleFactor;
    }

    private float transformRanges(float oldValue, float oldMin,
            float oldMax, float newMin, float newMax) {

        return newMin + (oldValue - oldMin) / (oldMax - oldMin) * (newMax - newMin);
    }

    private float transformFromHeightToOGL(float oldValue) {
        return transformRanges(oldValue, 0, WINDOW_HEIGHT, -1, 1);
    }

    private float transformFromOGLToHeight(float oldValue) {
        return transformRanges(oldValue, -1, 1, 0, WINDOW_HEIGHT);
    }

    private float transformFromWidthToOGL(float oldValue) {
        return transformRanges(oldValue, 0, WINDOW_WIDTH, -1, 1);
    }

    private float transformFromOGLToWidth(float oldValue) {
        return transformRanges(oldValue, -1, 1, 0, WINDOW_WIDTH);
    }
    
    private void translateToXY(float xPos, float yPos) {
        float scaledTranslateX = transformFromOGLToWidth(translateX);
        float scaledTranslateY = transformFromOGLToHeight(translateY);

        float valX = WINDOW_WIDTH / (2*scaleFactor);
        float valY = WINDOW_HEIGHT / (2*scaleFactor);  
             
        float rangeXNeg = scaledTranslateX - valX,
              rangeXPos = scaledTranslateX + valX,
              rangeYNeg = scaledTranslateY - valY,
              rangeYPos = scaledTranslateY + valY;                               
        
        float scaledXPos = transformRanges(xPos, 0, WINDOW_WIDTH, rangeXNeg, rangeXPos);
        float scaledYPos = transformRanges(yPos, 0, WINDOW_HEIGHT, rangeYPos, rangeYNeg);
                                                                    
        float tXPos = -transformFromWidthToOGL(scaledXPos);
        float tYPos = -transformFromHeightToOGL(scaledYPos);        
                
        translateX = -tXPos;
        translateY = -tYPos;        
    }

    public void zoomIn(GL4 gl, float xPos, float yPos) {
        translateToXY(xPos, yPos);                                             
        scaleFactor += scaleFactor / 10;               
        checkAndCorrectBounds();        
    }

    public void zoomOut(GL4 gl, float xPos, float yPos) {
        translateToXY(xPos, yPos);             
        scaleFactor -= scaleFactor / 10; 
        if (scaleFactor < 1.0f)
            scaleFactor = 1.0f;
        checkAndCorrectBounds();        
    }

    private void checkAndCorrectTranslateXBounds(float scaleBorder) {
        if (translateX < -scaleBorder) {
            translateX = -scaleBorder;
        }
        if (translateX > scaleBorder) {
            translateX = scaleBorder;
        }
    }

    private void checkAndCorrectTranslateYBounds(float scaleBorder) {
        if (translateY < -scaleBorder) {
            translateY = -scaleBorder;
        }
        if (translateY > scaleBorder) {
            translateY = scaleBorder;
        }
    }
    
    void checkAndCorrectBounds() {
        float scaleBorder = getScaleBorder();
        checkAndCorrectTranslateXBounds(scaleBorder);
        checkAndCorrectTranslateYBounds(scaleBorder);    
    }

    public void translateX(boolean right) {
        float scaleBorder = getScaleBorder();
        translateX += (right) ? (-0.02 / scaleFactor) : (0.02 / scaleFactor);
        checkAndCorrectTranslateXBounds(scaleBorder);
    }

    public void translateY(boolean up) {
        float scaleBorder = getScaleBorder();
        translateY += (up) ? (0.02 / scaleFactor) : (-0.02 / scaleFactor);
        checkAndCorrectTranslateYBounds(scaleBorder);
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        Mandelbrot.WINDOW_WIDTH = width;
        Mandelbrot.WINDOW_HEIGHT = height;
    }

    public static void main(String[] args) {

        GLProfile glp = GLProfile.getDefault();

        /*
        System.out.println(glp.getGLImplBaseClassName());
        System.out.println(glp.getImplName());
        System.out.println(glp.getName());
        System.out.println(glp.hasGLSL());
         */
        // Specifies a set of OpenGL capabilities, based on your profile.
        GLCapabilities caps = new GLCapabilities(glp);

        caps.setAlphaBits(8);
        caps.setDepthBits(24);
        //System.out.println(caps);

        // Create the OpenGL rendering canvas        
        GLWindow window = GLWindow.create(caps);

        // Create a animator that drives canvas' display() at the specified FPS.
        final FPSAnimator animator = new FPSAnimator(window, FPS, true);

        window.addWindowListener(new WindowAdapter() {
            @Override
            public void windowDestroyNotify(WindowEvent arg0) {
                // Use a dedicate thread to run the stop() to ensure that the
                // animator stops before program exits.
                new Thread() {
                    @Override
                    public void run() {
                        animator.stop(); // stop the animator loop
                        System.exit(0);
                    }
                }.start();
            }
        ;
        });
 
        Mandelbrot m = new Mandelbrot();
        window.addGLEventListener(m);
        window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);

        window.setTitle(TITLE);
        window.setVisible(true);
        window.addMouseListener(new MandelbrotMouseListener(m, window.getGL().getGL4()));
        animator.start();
    }

}
