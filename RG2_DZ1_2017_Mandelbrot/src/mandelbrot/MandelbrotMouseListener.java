package mandelbrot;

import com.jogamp.newt.event.MouseEvent;
import com.jogamp.newt.event.MouseListener;
import com.jogamp.opengl.GL4;

public class MandelbrotMouseListener implements MouseListener {

    private Mandelbrot mandelbrot;
    private GL4 gl;

    public MandelbrotMouseListener(Mandelbrot mandelbrot, GL4 gl) {
        this.mandelbrot = mandelbrot;
        this.gl = gl;
    }

    private int previousX = -1, previousY = -1;
    boolean justReleased = false;

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {        
        if (justReleased) {
            justReleased = false;
            return;
        }
        int newX = e.getX(),
            newY = e.getY();
        if (e.getButton() == MouseEvent.BUTTON1) {
            mandelbrot.zoomIn(gl, newX, newY);
        }
        if (e.getButton() == MouseEvent.BUTTON3) {
            mandelbrot.zoomOut(gl, newX, newY);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON3) {
            justReleased = true;
            return;
        }
        int newX = e.getX();
        int newY = e.getY();
        if (previousY != -1 && previousX != -1) {
            float diffRight = newX - previousX,
                    diffUp = newY - previousY;

            boolean right = diffRight > 0,
                    up = diffUp > 0;
            boolean shiftRight = Math.abs(diffRight) > 2.0f;
            boolean shiftUp = Math.abs(diffUp) > 2.0f;

            if (shiftRight) {
                mandelbrot.translateX(right);
            }
            if (shiftUp) {
                mandelbrot.translateY(up);
            }
            justReleased = true;
        }
        previousX = newX;
        previousY = newY;
    }

    @Override
    public void mouseWheelMoved(MouseEvent e) {
    }

}
