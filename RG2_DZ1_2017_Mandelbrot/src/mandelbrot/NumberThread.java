package mandelbrot;

import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class NumberThread extends Thread {

    private Scanner in;
    private boolean ready = false;

    private AtomicInteger maxIt = new AtomicInteger(10);

    public NumberThread() {
        in = new Scanner(System.in);
    }

    public int getMaxIt() {
        return maxIt.intValue();
    }

    @Override
    public void run() {
        System.out.print("Iterations: ");
        while (true) {
            int nextV = in.nextInt();            
            if (nextV > 360 || nextV < 0) {
                System.out.println("Num of iterations must be between 1 and 360!");
                System.out.print("Iterations: ");
            } else {
                maxIt.set(nextV);
                ready = true;
                System.out.print("Iterations: ");
            }

        }
    }
}
